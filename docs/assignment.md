# Varen dostop in Avtentikacija V6

Za lažje razumevanje vaj si poglejte vsebino predavanj <span class="sklop5">P5.1</span> [Varen dostop do API-ja, avtentikacija uporabnikov in sej](#Avtentikacija).

## Uvod

V tem trenutku imamo SPA aplikacijo **ComMENT TO BE**, 
ki ne razlikuje med uporabniki in dostop REST API-ju, ni varan. Izvorna koda trenutne aplikacije je dostopna na [<img src="img/git.png" class="git">pred-V6](https://bitbucket.org/spfri/pred-V6).

Pri tej nalogi bomo spremenili našo aplikacijo tako, da bo omogočala prijavo in registracijo.
Po tem bomo samo prijavljenom uporabniku omogučali dodavanje novih komentarjev in brisanje obstoječih.

Za osnovo lahko uporabite repozitorij  [<img src="img/git.png" class="git">V6](https://bitbucket.org/spfri/v6). Najbolje, da naredite svojo kopijo repozitorija (angl. fork) in ga preimenujete v `v6` pa izvedete naslednje ukaze:

~~~~ {.bash}
~/workspace $ git clone https://{študent}@bitbucket.org/{študent}/v6.git
~/workspace $ cd v6
~~~~

## Prijava in registracija

Za omogučavanje prijave in registracije 
naredite naslednje:

1. Dodajte shemu uporabnika `User` ki ima vsaj naslednje: (1) `name`, (2) `mail` 
in (3) geslo shranjeno v šifrirani obliki z uporabo `crypto`. 
2. Dodajte nove dostopne točke v REST API `/login` in `/register`. Te dostopne točke boste uporabili za obdelavo uporabniške prijave ali registracije. Ko se postopek prijave ali registracije uspešno konča, API mora poslati nazaj JWT žeton.
3. Dodajte poglede v aplikacijo, ki uporabniku omogoča prijavo ali registracijo. 
Ne pozabite narediti teh strani dostopnih, kadar je to primerno. Povezave do teh spletnih mest na primer ne potrebujemo, ko je uporabnik že prijavljen.

# Varen dostop do APIja

Spremenite REST API, tako da samo prijavljenom uporabniku, ki poleg vsebine zahteve posreduje in veljavni JWT žeton, omogočite API klic za dodajanje ali brisanje komentarja. Uporavite vmesni sloj in knjižnico `express-jwt`. Vmesni sloj uporabite za (1) preverjanje veljavnosti JWT žetona (avtentikacija), (2) pridobivanje podatkov iz žetona in dodajanje pridobljenih podatkov v objekt zahteve.

V krmilniku preverite, ali uporabnik obstaja in ima pravico dodajanja ali brisanja novega komentarja.


Izvorna koda rešitve naloge bo na voljo ob koncu tedna na [<img src="img/git.png" class="git">V6-resitev](https://bitbucket.org/mfilic/v6-resitev), objavljena pa na [drugo-ime238](https://drugo-ime238.herokuapp.com).